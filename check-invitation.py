#!/usr/bin/ssh-agent /usr/bin/python3

import gitlab
import pprint # useful for debugging
import argparse,getpass,re,time
from datetime import datetime
import sys,subprocess,os
import json,urllib.request

# Given a http or ssh git URL, return the repository name
# Example:
# url2reponame('gitlab@git.uwaterloo.ca:cs349-test1/johnsmith.git')
# => 'johnsmith'
def url2reponame(url):
    return url.rsplit('/',1)[-1][:-4]

# Parse command-line arguments.
parser = argparse.ArgumentParser(description="This script is used to clone student repositories.")
parser.add_argument('group_name', help="The name of the Gitlab group whose projects you want to clone.")
parser.add_argument('--token-file', default="/dev/stdin",
                    help="Path to file containing your Gitlab private token. Default is to read from standard input.")
args = parser.parse_args()

# save command line argument inputs in variables
group_to_clone = args.group_name
token_file = args.token_file

# Read private token from keyboard or from file
gitlab.set_private_token(token_file)

#
# Get the ID of group_to_clone
# ID will be stored in group_id
#

print("Getting ID of group %s." % group_to_clone)
group_id = gitlab.get_group_id(group_to_clone)
print("Found group %s which has ID %d" % (group_to_clone, group_id))

print("Getting git repo URLs in group %s (id %d)." % (group_to_clone, group_id))

projects_data = gitlab.request("groups/%d/projects?order_by=name&sort=asc&per_page=10000" % group_id)
all_usernames = []
urls = []
for project in projects_data:
    http_url = project['http_url_to_repo'] 
    ssh_url = project['ssh_url_to_repo']
    username = url2reponame(ssh_url)
    all_usernames.append(username)
    urls.append({'username': username,
                 'project_id': project['id'],
                 'http_url': http_url,
                 'ssh_url': ssh_url})
    members = gitlab.request("projects/%s/members" % project['id'])
    if len(members) >= 1 and members[0] != None:
        print("%s,%s,%s,%s" % (username,ssh_url,http_url,"accepted"))
    else:
        print("%s,%s,%s,%s" % (username,ssh_url,http_url,"DID NOT ACCEPT INVITE YET"))
